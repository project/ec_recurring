<?php

/**
 * @file
 * Provide views integration for ec_recurring
 */

/**
 * Implementation of hook_views_data().
 */
function ec_recurring_views_data() {
  $data = array();
  
  $data['ec_recurring_expiration']['table']['group'] = t('Transaction');
  $data['ec_recurring_expiration']['table']['join'] = array(
    'ec_transaction' => array(
      'left_field' => 'txnid',
      'field' => 'txnid',
      'type' => 'LEFT',
    ),
  );
  
  $data['ec_recurring_expiration']['is_recurring'] = array(
    'title' => t('Is recurring'),
    'help' => t('TODO: Add help'),
    'filter' => array(
      'handler' => 'ec_recurring_views_handler_filter_is_recurring',
    ),
  );
  $data['ec_recurring_expiration']['start_time'] = array(
    'title' => t('Start time'),
    'help' => t('TODO: Add help'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  $data['ec_recurring_expiration']['expiry'] = array(
    'title' => t('Expiry'),
    'help' => t('TODO: Add help'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  $data['ec_recurring_expiration']['status'] = array(
    'title' => t('Recuring status'),
    'help' => t('TODO: Add help'),
    'field' => array(
      'handler' => 'ec_recurring_views_handler_field_status',
      'click sortable' => TRUE,
    ),
  );
  
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function ec_recurring_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'ec_recurring') .'/views',
    ),
    'handlers' => array(
      'ec_recurring_views_handler_filter_is_recurring' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
      'ec_recurring_views_handler_field_status' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
