<?php

/**
 * Implementation of hook_feature_load().
 */
function ec_recurring_feature_load(&$node) {
  module_load_include('inc', 'ec_recurring');
  ec_recurring_schedule_node_load($node);
}

/**
 * Implementation hook_feature_insert()
 */
function ec_recurring_feature_insert($node) {
  ec_recurring_product_save($node);
}

/**
 * Implementation hook_feature_update()
 */
function ec_recurring_feature_update($node) {
  ec_recurring_product_save($node);
}

/**
 * Implementation of e-Commerce hook_feature_ec_checkout_validate_item()
 */
function ec_recurring_feature_ec_checkout_validate_item(&$node, $type, $qty, $data, $severity, $return) {
  global $user;

  if (isset($node->schedule) && is_array($node->schedule)) {
    return ec_recurring_product_allowed_in_cart($node, $user->uid, ($severity >= EC_VALIDATE_ITEM_SEVERITY_MEDIUM) === FALSE);
  }
  else {
    return TRUE;
  }
}

/**
 * Implementation of hook_feature_attributes().
 */
function ec_recurring_feature_attributes($node) {
  return array('no_cart' => TRUE);
}

function ec_recurring_feature_product_checkout_init(&$txn, $item) {  
  if ($txn->type != 'ec_recurring') {
    $txn->recurring = 1;
    $txn->receipts['payment_filter']['allow_recurring'] = TRUE;
  }
}

/**
 * Implementation of hook_product_calculate().
 */
function ec_recurring_feature_product_calculate(&$txn, $item) {
  // If the schedule is not set then treat it as a normal product.
  if (empty($item->schedule)) {
    return;
  }
  if ($txn->type != 'ec_recurring') {
    $params = array(
      'trial' => TRUE,
      'ecid' => isset($txn->ecid) ? $txn->ecid : $txn->customer->ecid,
      'items' => array($item->nid => $item),
    );

    if ($recurring_txn = ec_checkout_create_transaction('ec_recurring', $params)) {
      module_load_include('inc', 'ec_recurring');
      
      $txn->recurring = array(
        'price' => $recurring_txn->gross,
      );
      
      $txn->additional['ec_recurring'] = array(
        '#prefix' => '<div class="recurring-message">',
        '#suffix' => '</div>',
        '#value' => t('Scheduled payments of %price will be made from %date', array('%price' => format_currency($txn->recurring['price']), '%date' => format_date(_ec_recurring_expiry_calc($item->schedule), 'small'))),
      );
    }
  }
}

/**
 * Implementation of hook_feature_transaction_load().
 */
function ec_recurring_feature_transaction_load($item) {
  module_load_include('inc', 'ec_recurring');
  ec_recurring_schedule_node_load($item);
  
  return array('schedule' => $item->schedule, 'is_recurring' => $item->is_recurring);
}

/**
 * Implementation of hook_feature_transaction_delete().
 */
function ec_recurring_feature_transaction_delete($item) {
  db_query('DELETE FROM {ec_recurring_expiration} WHERE txnid = %d', $item->txnid);
}