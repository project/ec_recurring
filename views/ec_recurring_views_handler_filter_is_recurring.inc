<?php

/**
 * @file
 * Provide handler to allow users to filter out recurring transactions
 */

class ec_recurring_views_handler_filter_is_recurring extends views_handler_filter_boolean_operator {
  function query() {
    $this->ensure_my_table();
    $this->query->add_where($this->options['group'], $this->table_alias .'.txnid '. (empty($this->value) ? 'IS ' : 'IS NOT ') .'NULL');
  }
}