<?php

/**
 * @file
 * Provide handler to display the status of a recurring invoice
 */

class ec_recurring_views_handler_field_status extends views_handler_field {
  function render($values) {
    return theme('recurring_status', $values->{$this->field_alias});
  }
}