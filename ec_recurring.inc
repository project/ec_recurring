<?php
/**
 * @file
 * Enables recurring product support in Drupal e-Commerce.
 * @author Sammy Spets (thanks to Synerger Pty Ltd, Australia)
 */

/**
 * Validates the schedule given to ensure it has valid values for unit and
 * numunits. If an error is found, the function calls exit(). This is used
 * because errors of this sort are programming errors and not user errors.
 *
 * @param $fname
 *   String, Name of the calling function
 * @param $schedule
 *   Array or object, Containing a unit and numunits.
 * @return
 *   Array (unit, numunits). This function fails (calls exit()) if
 * any errors are found.
 */
function _ec_recurring_calc_validate_input($fname, &$schedule) {
  $errors = array();

  if (is_array($schedule)) {
    list($unit, $numunits) = array($schedule['unit'], $schedule['numunits']);
  }
  elseif (is_object($schedule)) {
    list($unit, $numunits) = array($schedule->unit, $schedule->numunits);
  }
  else {
    $errors[] = "$fname(): ERROR. Expected array or object for parameter 1";
  }

  $msg = '';
  if ($fname != '_ec_recurring_reminder_time_calc') {
    if (empty($unit)) {
      $msg = 'unit';
    }
    elseif (empty($numunits)) {
      $msg = 'numunits';
    }
  }

  if (!empty($msg)) {
    $errors[] = "$fname(): ERROR. Expected schedule to have '$msg' defined";
  }

  if (!in_array($unit, array_keys(_ec_recurring_get_units()))) {
    $errors[] = "$fname(): ERROR. Unit \"$unit\" invalid";
  }

  if (!empty($errors)) {
    echo implode('<br/>', $errors) .'<br/>';
    exit(1);
  }

  return array($unit, $numunits);
}

/**
 * Inserts the given entry into the ec_recurring_expiration table
 *
 * @param $entry
 *   Array, txnid, start_time, vid, rid, expiry
 *   and optionally, the status (defaults to ECRECURRING_STATUS_ACTIVE)
 */
function _ec_recurring_expiration_insert(&$entry) {
  ec_recurring_expiration_entry_validate($entry, __FUNCTION__);
  
  drupal_write_record('ec_recurring_expiration', $entry);
  return db_affected_rows();
}

/**
 * Inserts reminder entries into the ec_recurring_expiration table. Corresponding
 * product entries are found using the supplied schedule id. Returns the number
 * of records changed.
 *
 * @param $sid ID of the schedule that has changed
 * @param $rid ID of the new reminder added to the schedule
 * @param $numunits Number of units
 * @param $unit Units used to calculate the reminder expiry time
 * @return Number of records changed
 */
function _ec_recurring_expiration_insert_reminders($sid, $rid, $numunits, $unit) {
  $s = array('rid' => $rid, 'numunits' => $numunits, 'unit' => $unit);
  return ec_recurring_expiration_map($sid, 'ec_recurring_expiration_mapfn_insert_reminder', ECRECURRING_MAP_PRODUCT_BY_SID, $s);
}

/**
 * Returns the local timestamp (unix timestamp) for the expiry desired
 * in schedule. Expiry is calculated from midnight for the given $prevexpiry.
 * If $prevexpiry is NULL, the current unix timestamp is used.
 *
 * @param $schedule Reference to the schedule array or object.
 * @param $prevexpiry Timestamp of the previous expiry date. Can be NULL.
 * @return Returns a timestamp for the new expiry date. If $schedule is passed
 * as neither an array or object this function calls exit(). Likewise, if
 * values for unit and numunits are not found in $schedule, exit() is called.
 */
function _ec_recurring_expiry_calc(&$schedule, $prevexpiry = NULL) {
  if (is_null($prevexpiry)) {
    $prevexpiry = _ec_recurring_expiry_time_default();
  }

  list($unit, $numunits) = _ec_recurring_calc_validate_input(__FUNCTION__, $schedule);

  $prevexpiry = date('Y-m-d 03:00:00', $prevexpiry);

  $units = ec_recurring_unit_string($unit, $numunits, TRUE);

  return strtotime("$prevexpiry +$units");
}

/**
 * Returns the default time used when performing calculations. Use this function
 * rather than hard-coding the call to mktime() as this implementation may change
 * to take timezones into account.
 *
 * @return Timestamp for 3am on the current date in system local time
 */
function _ec_recurring_expiry_time_default($time = NULL) {
  if (is_null($time) || $time === FALSE) {
    return mktime(3, 0, 0);
  }
  else {
    return strtotime(date('Y-m-d 03:00:00', $time));
  }
}

/**
 * List all products with active recurring schedule
 *
 * @param $uid
 *   Number, the User ID
 * @return
 *   Array, All products with active recurring schedule
 */
function _ec_recurring_get_active($ecid) {
  $active = array();
  
  $result = db_query('SELECT * FROM {ec_recurring_expiration} e,
    {ec_recurring_schedule} r,
    {ec_product} p,
    {ec_transaction} t,
    {node_revisions} n,
    {ec_transaction_product} etp
    WHERE e.txnid = t.txnid
    AND n.vid = e.vid
    AND r.sid = p.sid
    AND p.vid = e.vid
    AND p.vid = etp.vid
    AND e.txnid = etp.txnid
    AND t.ecid = %d
    AND e.status = %d
    AND e.rid <= %d',
    $ecid, ECRECURRING_STATUS_ACTIVE, ECRECURRING_UNLIMITED);

  while ($row = db_fetch_array($result)) {
    $active[] = $row;
  }

  return $active;
}

/**
 * Returns a list of cycle values ready for use in a select form element.
 *
 * @note This system stores the number of cycles for an expiration
 * and not the number of renewals. E.g An expiration with one cycle has no
 * renewals whereas an expiration with two cycles has one renewal and so on.
 *
 * @return
 *   Array, cycle values for use in a select form element.
 */
function _ec_recurring_get_cycles() {
  static $cycles;
  if (empty($cycles)) {
    $cycles[ECRECURRING_UNLIMITED] = t('Unlimited');
    $cycles[ECRECURRING_NORENEWAL] = t('No renewals');
    $cycles[2] = t('1 renewal');
    for ($i = 2; $i <= ECRECURRING_MAXCYCLES; $i++) {
      $cycles[$i+1] = t('!i renewals', array('!i' => $i));
    }
  }
  return $cycles;
}

/**
 * Returns an array of all recurring schedules ready for use
 * in a select form element.
 *
 * @param $show_renewable
 *   Boolean, Set to TRUE if renewable schedules can be included
 * in the returned list; otherwise FALSE.
 * @return
 *   Array, All schedules ready for use in a select form element.
 */
function _ec_recurring_get_list($show_renewable = TRUE) {
  if ($show_renewable) {
    $result = db_query("SELECT * FROM {ec_recurring_schedule} WHERE cycles <> %d ORDER BY sid", ECRECURRING_NORENEWAL);
  }
  else {
    $result = db_query("SELECT * FROM {ec_recurring_schedule} ORDER BY sid");
  }
  $list = array();
  while ($row = db_fetch_array($result)) {
    $list[$row['sid']] = $row['name'] .': '. theme('recurring_schedule', $row);
  }
  return $list;
}

/**
 * Returns the mail ID for the given reminder ID. This function
 * caches the mail IDs found to speed up processing for cron jobs.
 *
 * @param $rid
 *   Number, Reminder ID
 * @return
 *   Number, Mail ID for the given reminder ID
 */
function _ec_recurring_get_mid($rid) {
  static $midlist;

  if (!isset($midlist)) {
    $midlist = array();
  }

  if (empty($midlist[$rid])) {
    $midlist[$rid] = db_result(db_query('SELECT mid FROM {ec_recurring_reminder} WHERE rid = %d', $rid));
  }

  return $midlist[$rid];
}

/**
 * Returns an array of numunits suitable for use in a form.
 *
 * @return
 *   Array, numunits => numunits pairs.
 */
function _ec_recurring_get_numunits() {
  static $numunits;
  if (empty($numunits)) {
    $numunits[''] = '--';
    $numunits += drupal_map_assoc(range(0, ECRECURRING_MAXINTERVAL));
  }
  return $numunits;
}

/**
 * Returns schedules stored in the system as an array of rows. Each row can be
 * optionally manipulated by the supplied row function to alter the resulting
 * set.
 *
 * @param $tablename
 *   String, Name of the database table to process
 * @param $rowfn
 *   String, Function which alters each row. Calling syntax must be
 *   rowFn(&$row).
 * @param $orderfield
 *   String, Name of the field(s) for ordering the rows. Multiple fields
 *   can be specified by separating them with commas. E.g 'field1, field2'.
 * @param $qry_override
 *   String, Override of the default query. Use this to do multitable
 *   queries.
 * @return
 *   Array, Defaults to all rows but this may be changed by using
 *   a row function.
 */
function _ec_recurring_get_table($tablename, $rowfn = '', $orderfield = '', $qry_override = '') {
  $orderby = !empty($orderfield) ? " ORDER BY $orderfield" : '';

  $result = db_query(empty($qry_override) ? 'SELECT * FROM {'. $tablename ."}$orderby" : $qry_override . $orderby);
  $table = array();
  while ($row = db_fetch_array($result)) {
    if (!empty($rowfn) && function_exists($rowfn)) {
      $table[] = $rowfn($row);
    }
    else {
      $table[] = $row;
    }
  }
  return $table;
}

/**
 * Populates the schedule member of the node object given.
 * If a schedule is not found for the given node's version the
 * schedule member remains unset.
 *
 * @param $node
 *   Object, Reference to the node for which the schedule is desired.
 */
function ec_recurring_schedule_node_load(&$node) {
  // get the expiry date if the product has been purchased before. if not, the system
  // will calculate all the expiry dates according to the current date
  if (isset($node->txnid) && !empty($node->txnid) && !empty($node->vid)) {
    $expiry = ec_recurring_product_get_expiry($node->txnid, $node->vid);
    $extend = FALSE;
  }

  if (empty($expiry)) {
    $expiry = FALSE;
    $extend = TRUE;
  }

  if (!empty($node->vid)) {
    if (isset($node->sid) && $node->sid) {
      $schedule = ec_recurring_schedule_load($node->sid, $expiry, $extend);
    }
    else {
      $schedule = ec_recurring_schedule_vid_load($node->vid, $expiry, $extend);
    }

    if (is_array($schedule)) {
      $node->schedule = $schedule;
      $node->is_recurring = TRUE; // for backward compatibility
    }
  }
}

/**
 * Implementation of a expiration map function to expire a product row.
 * Be sure that only a product row is passed to this function!
 */
function _ec_recurring_mapfn_expire_product(&$row, &$data) {
  static $products;

  if ($row['rid'] > ECRECURRING_UNLIMITED) {
    return;
  }

  if (!is_array($products)) {
    $products = array();
  }

  if (!isset($products[$row['vid']])) {
    $n = node_load(array('nid' => $row['nid']), $row['vid']);
    $n->txnid = $row['txnid'];
    $n->ecid = $row['ecid'];
    $products[$row['vid']] = $n;
  }

  $products[$row['vid']]->expired_schedule = $row;

  foreach (module_implements('recurringapi') as $module) {
    $function = $module . '_recurringapi';
    $function('on expiry', $products[$row['vid']], $data['expiry_time']);
  }

  // watchdog is done in the hook implementation rather than here :)
}

/**
 * Implementation of a expiration map function to expire a reminder row.
 * Be sure that only a reminder row is passed to this function!
 *
 * @param &$row
 *   Array, The rows
 * @param &$data
 *   Array, its not used
 */
function  ec_recurring_mapfn_expire_reminder(&$row, &$data) {
  global $_ec_recurring_expirations;

  if ($row['rid'] <= ECRECURRING_UNLIMITED) {
    return;
  }

  if ($row['status'] == ECRECURRING_STATUS_ACTIVE) {
    include_once drupal_get_path('module', 'ec_recurring') .'/ec_recurring.inc';

    // send the mail
    $address = ec_mail_get_transaction_address($row['txnid']);
    $mid = ec_recurring_get_mid($row['rid']);
    $txn = store_transaction_load($row['txnid']);
    $txn->recurring = $row;
    if (!empty($address) && ec_mail_send_mid($mid, $address, $txn)) {
      watchdog('reminder_sent', 'Reminder mail sent.', array(), WATCHDOG_NOTICE, l(t('view reminder'), 'store/transaction/view/'. $row['txnid']));
      $_ec_recurring_expirations['reminders_sent']++;
      $_ec_recurring_expirations['reminder_addresses'][] = $address;
    }
    else {
      $_ec_recurring_expirations['reminders_failed']++;
      $_ec_recurring_expirations['reminder_badaddresses'][] = (empty($address) ? "txnid: {$row['txnid']}" : $address);
    }
  }

  $row['status'] = ECRECURRING_STATUS_EXPIRED;
  _ec_recurring_expiration_update($row);
}

/**
 * Populates an array with reminder entries suitable for direct insertion into or update of
 * the ec_recurring_expiration table.
 *
 * @param $entries
 *   Array, The entries are added. Their index corresponds to the
 *   index in the given schedule
 * @param $pentry
 *   String, Product entry that will be inserted/updated in
 *   ec_recurring_expiration
 * @param $schedule
 *   Array, Schedule upon which the entries are calculated
 * @param $start_time
 *   Number, Time from which reminder expiries are calculated (i.e time at which
 * the product entry expires)
 */
function _ec_recurring_reminder_entries_build(&$entries, &$pentry, &$schedule, $start_time) {
  $r =& $schedule['reminders'];
  for ($i = 0; $i < count($r); $i++) {
    $entries[$i] = $pentry; // copy the structure
    $entries[$i]['start_time'] = $start_time;
    $entries[$i]['rid'] = $r[$i]['rid'];
    $entries[$i]['expiry'] = $r[$i]['expiry'];
  }
}

/**
 * Returns a local timestamp (unix timestamp) of when a reminder will be sent.
 * The timestamp is calculated as being before $expiry, which is the timestamp
 * of the expiry date. The timestamp returned will be at midnight on the day
 * calculated.
 *
 * @return
 *   String, Local timestamp of when the reminder is to be sent.
 */
function _ec_recurring_reminder_time_calc(&$schedule, $expiry) {
  list($unit, $numunits) = _ec_recurring_calc_validate_input(__FUNCTION__, $schedule);

  $expiry = _ec_recurring_expiry_time_default($expiry);
  $units = ec_recurring_unit_string($unit, $numunits, TRUE);

  if ($numunits == ECRECURRING_ATEXPIRY_NUMUNITS) {
    return strtotime($expiry);
  }
  else {
    return strtotime("$expiry -$units");
  }
}

/**
 * Updates all reminders with the new schedule. Returns the number of reminders
 * modified.
 *
 * @param $rid
 *   Number, Reminder ID
 * @param $numunits
 *   Number, New numunits
 * @param $unit
 *   Number, New unit
 * @return
 *   Number, Reminders modified.
 */
function _ec_recurring_reminders_update_all($rid, $numunits, $unit) {
  $s = array('numunits' => $numunits, 'unit' => $unit);
  return ec_recurring_expiration_map($rid, 'ec_recurring_expiration_mapfn_update_reminder', ECRECURRING_MAP_REMINDER_BY_RID, $s);
}


