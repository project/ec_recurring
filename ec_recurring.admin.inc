<?php
/**
 * @file
 * Enables recurring product support in Drupal e-Commerce.
 */

function ec_recurring_cron_settings() {
  $hours = drupal_map_assoc(range(0, 23), '_ec_recurring_int_to_hour');

  $form['ec_recurring_expiry_hour'] = array(
    '#type' => 'select',
    '#title' => t('Hour of expiry'),
    '#default_value' => variable_get('ec_recurring_expiry_hour', ECRECURRING_EXPIRY_HOUR),
    '#options' => $hours,
    '#description' => t('Select the hour of the day at which the cron system will expire products. NOTE: This only works if you have setup a cron task.')
  );

  return system_settings_form($form);
}

function ec_recurring_reminder_form(&$form_state, $s, $rid = '') {
  $r = NULL;
  if (is_numeric($rid) && $rid) {
    for ($i = 0; $i < count($s['reminders']); $i++) {
      if ($s['reminders'][$i]['rid'] == $rid) {
        $r =& $s['reminders'][$i];
        break;
      }
    }
    if (is_null($r)) {
      return drupal_not_found();
    }
  }

  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $r['rid']
  );
  $form['prodexpiry'] = array(
    '#type' => 'item',
    '#title' => t('Product expiry time'),
    '#value' => theme('recurring_schedule', $s)
  );
  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $s['sid'],
  );

  $numunits = _ec_recurring_get_numunits();
  $numunits[ECRECURRING_ATEXPIRY_NUMUNITS] = t('At product expiry');
  $form['numunits'] = array(
    '#type' => 'select',
    '#title' => t('Numunits'),
    '#options' => $numunits,
    '#default_value' => ($r ? $r['numunits'] : ''),
    '#description' => t('Number of units before expiry of the product. Select <b>at expiry</b> if you want the mail sent when the product expires. You can select any unit for mails sent at expiry.')
  );
  $form['unit'] = array(
    '#type' => 'select',
    '#title' => t('Unit'),
    '#options' => _ec_recurring_get_units(),
    '#default_value' => ($r ? $r['unit'] : '')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => (is_null($r) ? t('Add reminder') : t('Update reminder'))
  );

  return $form;
}

function ec_recurring_reminder_form_validate(&$form, &$form_state) {
  if (!is_numeric($form_state['values']['numunits'])) {
    form_set_error('numunits', t('Please select an numunits'));
  }

  if ($form_state['values']['numunits'] != ECRECURRING_ATEXPIRY_NUMUNITS) {
    $units = array_keys(_ec_recurring_get_units());
    array_shift($units);
    if (!in_array($form_state['values']['unit'], $units)) {
      form_set_error('unit', t('Please select a unit'));
    }
  }
  else {
    $form_state['values']['unit'] = ECRECURRING_ATEXPIRY_UNITS;
  }
}

function ec_recurring_reminder_form_submit(&$form, &$form_state) {
  $msg = '';
  if ($form_state['values']['numunits'] == ECRECURRING_ATEXPIRY_NUMUNITS) {
    $form_state['values']['unit'] = ECRECURRING_ATEXPIRY_UNITS;
  }
  $rid = ec_recurring_save_reminder($form_state['values'], $msg);

  $op = (empty($form_state['values']['rid']) ? t('added') : t('updated'));
  $msg = t('Reminder email %op. !msg', array('%op' => $op, '!msg' => $msg));
  drupal_set_message($msg);
  watchdog('ec_recurring', $msg, array(), WATCHDOG_NOTICE, l(t('edit'),
    "admin/ecsettings/schedule/{$form_state['values']['sid']}/reminder/$rid/edit"));

  $form_state['redirect'] = "admin/ecsettings/schedule/{$form_state['values']['sid']}/edit";
}


/**
 * Delete schedule
 *
 * @param $sid
 *  Sid of schedule that is going to be deleted.
 */
function _ec_recurring_schedule_delete($sid) {
  $result = db_query('SELECT rid FROM {ec_recurring_reminder} WHERE sid = %d', $sid);
  while ($row = db_fetch_object($result)) {
    ec_recurring_reminder_delete($row->rid);
  }
  db_query('DELETE FROM {ec_recurring_schedule} WHERE sid = %d', $sid);
}

function ec_recurring_schedule_delete_form(&$form_state, $s) {
  $form = array();
  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $s['sid'],
  );
  return confirm_form($form, t('Are you sure you want to delete %name', array('%name' => $s['name'])), "admin/ecsettings/schedule");
}

function ec_recurring_schedule_delete_form_submit(&$form, &$form_state) {
  _ec_recurring_schedule_delete($form_state['values']['sid']);
  $form_state['redirect'] = 'admin/ecsettings/schedule';
}

/**
 * Displays a table of schedules currently stored in the system.
 *
 * @return HTML of the table containing list of schedules stored in the system.
 */
function ec_recurring_table() {
  module_load_include('inc', 'ec_recurring');
  
  $output = '';
  $head = array(t('Name'), t('Schedule'), array('data' => t('Operations'), 'colspan' => 2));
  $rows = array();

  $result = db_query('SELECT * FROM {ec_recurring_schedule} ORDER BY name');
  while ($row = db_fetch_array($result)) {
    $rows[] = array(
      $row['name'],
      theme('recurring_schedule', $row),
      l(t('Edit'), "admin/ecsettings/schedule/{$row['sid']}/edit", array('query' => drupal_get_destination())),
      ec_recurring_schedule_can_delete($row['sid']) ? l(t('Delete'), "admin/ecsettings/schedule/{$row['sid']}/delete", array('query' => drupal_get_destination())) : '',
    );
  }

  if (empty($rows)) {
    $output .= t('No schedules were found');
  }
  else {
    $output .= theme('table', $head, $rows);
  }

  return $output;
}

/**
 * Form API code for displaying the schedule add/update form.
 *
 * @param $sid ID of the schedule being edited. If this is not supplied
 * a schedule add form will be displayed.
 * @return Form API array.
 */
function ec_recurring_schedule_form(&$form_state, $s = NULL) {
  $mail_list = array();
  if (!empty($s)) {
    foreach ($s['reminders'] as $r) {
      $links = array();
      $links['ec_sch_edit'] = array(
        'title' => t('Edit'),
        'href' => "admin/ecsettings/schedule/{$s['sid']}/reminder/{$r['rid']}/edit"
      );
      $links['ec_sch_delete'] = array(
        'title' => t('Delete'),
        'href' => "admin/ecsettings/schedule/{$s['sid']}/reminder/{$r['rid']}/delete"
      );
      $mail_list[] = array(l($r['name'], "admin/ecsettings/ec_mail/{$r['mid']}"), theme('recurring_schedule', $r), theme('recurring_date', $r['expiry']), theme('links', $links));
    }
  }
  $units = _ec_recurring_get_units();

  $readonly = ($s && ec_recurring_schedule_has_unexpired($s['sid']) ? TRUE : FALSE);

  // FIXME: $form_state is not set
  if (!isset($form_state['values']['op']) && $readonly) {
    $date = db_result(db_query_range('SELECT e.expiry FROM {ec_recurring_expiration} e,
      {ec_product} p WHERE e.vid = p.vid AND p.sid = %d AND e.rid <= %d
      AND e.status <> %d ORDER BY e.expiry DESC', $s['sid'],
      ECRECURRING_UNLIMITED, ECRECURRING_STATUS_EXPIRED, 0, 1));
    $date = theme('recurring_date', $date);
    drupal_set_message(t('<b>WARNING:</b> Some unexpired products exist that use this schedule. Only name changes are possible until %date', array('%date' => $date)));
  }

  $form['sid'] = array(
    '#type' => 'value',
    '#value' => ($s ? $s['sid'] : '')
  );
  $form['readonly'] = array(
    '#type' => 'value',
    '#value' => $readonly
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#maxlength' => ECRECURRING_NAMELEN,
    '#default_value' => ($s ? $s['name'] : ''),
    '#required' => TRUE,
  );
  module_load_include('inc', 'ec_recurring');
  $numunitss = _ec_recurring_get_numunits();
  $form['numunits'] = array(
    '#type' => ($readonly ? 'textfield' : 'select'),
    '#title' => t('Number of units'),
    '#default_value' => _ec_recurring_get_default_value($s, 'numunits', $readonly, $numunitss),
    '#options' => $numunitss,
    '#disabled' => $readonly,
    '#required' => TRUE,
  );

  $form['unit'] = array(
    '#type' => ($readonly ? 'textfield' : 'select'),
    '#title' => t('Unit'),
    '#default_value' => _ec_recurring_get_default_value($s, 'unit', $readonly, $units),
    '#options' => $units,
    '#required' => TRUE,
  );
  
  $cycles = _ec_recurring_get_cycles();
  $form['cycles'] = array(
    '#type' => ($readonly ? 'textfield' : 'select'),
    '#title' => t('Number of renewals'),
    '#default_value' => _ec_recurring_get_default_value($s, 'cycles', $readonly, $cycles),
    '#description' => t('Select the number of renewals this schedule has.'),
    '#options' => $cycles,
  );

  if (!empty($s)) {
    $form['expiry'] = array(
      '#type' => 'item',
      '#title' => t('Example expiry date'),
      '#value' => ($s ? date('Y-m-d', $s['expiry']) : '')
    );

    /*
    $form['mlist'] = array(
      '#type' => 'item',
      '#title' => t('Reminder mails'),
      '#value' => (empty($mail_list) ? t('No reminders added to schedule. <a href="!url">Add one</a>.', array('!url' => url("admin/ecsettings/schedule/{$s['sid']}/reminder/add"))) : theme('table', array(t('Name'), t('Schedule'), t('Example Date'), t('Operations')), $mail_list))
    );
    $form['reminders'] = array(
      '#type' => 'value',
      '#value' => $s['reminders']
    ); */
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => ($s ? t('Update schedule') : t('Add schedule'))
  );
  
  if ($s) {
    $form_state['redirect'] = 'admin/ecsettings/schedule';
  }
  
  return $form;
}

/**
 * Validates the schedule add/edit form.
 *
 * @param $form_id Text ID of the form
 * @param $form_values Reference to the values supplied by the user
 */
function ec_recurring_schedule_form_validate(&$form, &$form_state) {
  if (empty($form_state['values']['readonly'])) {
    $units = array_keys(_ec_recurring_get_units());
    array_shift($units);
    if (!in_array($form_state['values']['unit'], $units)) {
      form_set_error('unit', t('Please select a unit'));
    }
  }

  // Check reminder validity. Ensure they are between purchase and
  // expiry
  /* for ($i = 0; $i < count($form_state['values']['reminders']); $i++) {
    $r =& $form_state['values']['reminders'][$i];
    if ($r['expiry'] < $form_state['values']['expiry']) {
      form_set_error('unit', t('Unit selected is too small for the reminder schedules to be valid'));
    }
  } */
}

/**
 * Commits schedule fields supplied by the user.
 *
 * @param $form_id Text ID of the form
 * @param $form_values Reference to the values supplied by the user
 * @return URL to which the user is redirected once data is committed
 */
function ec_recurring_schedule_form_submit(&$form, &$form_state) {
  $add = empty($form_state['values']['sid']);
  $msg = (empty($form_state['values']['sid']) ? t('Schedule added') : t('Schedule updated')) .": {$form_state['values']['name']}";

  $extramsg = _ec_recurring_schedule_save($form_state['values']);
  $sid = $form_state['values']['sid'];

  $msg .= ". $extramsg";
  watchdog('ec_recurring', $msg, array(), WATCHDOG_NOTICE, l(t('Edit'), "admin/ecsettings/schedule/$sid/edit"));
  drupal_set_message($msg);
  return $add ? "admin/ecsettings/schedule/$sid/edit" : "admin/ecsettings/schedule";
}

