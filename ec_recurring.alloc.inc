<?php
/**
 * @file
 * Implements the recurring aspects of the allocation interface.
 */

/**
 * Implementation of hook_alloc_get_recurring().
 */
function ec_recurring_alloc_get_recurring($txn) {
  if (isset($txn->recurring) && $txn->recurring) {
    if ($recurring_products = array_filter($txn->items, 'ec_recurring_filter_recurring_products')) {
      module_load_include('inc', 'ec_recurring');
      
      // Only use the first recurring product as there should only ever be one recurring product in
      // in a transaction.
      $item = reset($recurring_products);
      
      $recurring = array(
        'id' => $item->nid,
        'start_date' => _ec_recurring_expiry_calc($item->schedule),
        'price' => $txn->recurring['price'],
        'period' => $item->schedule['numunits'],
        'unit' => $item->schedule['unit'],
        'description' => theme('recurring_schedule', $item->schedule),
      );
      
      if ($item->schedule['cycles'] >= 1) {
        $recurring['repeat'] = $item->schedule['cycles'];
      }
      
      return $recurring;
    }
  }
}

/**
 * Implementation of hook_get_recurring_enddate().
 */
function ec_recurring_alloc_get_recurring_enddate($txn, $recurring) {
  if (isset($txn->recurring) && $txn->recurring) {
    if ($recurring_products = array_filter($txn->items, 'ec_recurring_filter_recurring_products')) {
      module_load_include('inc', 'ec_recurring');
      
      // Only use the first recurring product as there should only ever be one recurring product in
      // in a transaction.
      $item = reset($recurring_products);
      
      if (isset($recurring['repeat'])) {
        $repeats = $recurring['repeat'] - 1; // -1 since we are starting from the first recurring payment.
        $enddate = $recurring['start_date'];
        
        while ($repeats > 0) {
          $enddate = _ec_recurring_expiry_calc($item->schedule, $enddate);
          $repeats--;
        }
        
        return $enddate;
      }
    }
  }
}

/**
 * Implementation of hook_alloc_update_recurring_payment().
 */
function ec_recurring_alloc_update_recurring_payment($txn, $receipt) {
  if ($recurring_products = array_filter($txn->items, 'ec_recurring_filter_recurring_products')) {
    // Only use the first recurring product as there should only ever be one recurring product in
    // in a transaction.
    $item = reset($recurring_products);
    
    $params = array(
      'ecid' => isset($txn->ecid) ? $txn->ecid : $txn->customer->ecid,
      'items' => array($item->nid => $item),
    );
    
    if ($recurring_txn = ec_checkout_create_transaction('ec_recurring', $params)) {
      $alloc[] = array(
        'type' => 'transaction',
        'id' => $recurring_txn->txnid,
      );
      ec_receipt_allocate($receipt, $alloc);
    }
  }
}